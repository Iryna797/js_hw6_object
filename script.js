function createNewUser() {
  const newUser = new Object();
    
    newUser.firstName = prompt("Введите имя");
  
    while (newUser.firstName === "" || newUser.firstName == null) {
     newUser.firstName = prompt("Введите имя");
    }
   
    newUser.lastName = prompt("Введите фамилию");
    while (newUser.lastName === "" || newUser.lastName == null) {
      newUser.lastName = prompt("Введите фамилию");
    }
   
    newUser.birthday = prompt("Введите дату рождения", "dd.mm.yyyy");
    while (newUser.birthday === "" || newUser.birthday == null || newUser.birthday.length < 10) {
      newUser.birthday = prompt("Введите дату рождения");
    }
   
    newUser.getPassword = function () {
     return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6, 10));
    };
   
    newUser.getAge = function () {
     const date = new Date();
     const dd = Number(this.birthday.slice(0, 2));
     const mm = Number(this.birthday.slice(3, 5));
     const yyyy = Number(this.birthday.slice(6));
     const isBirthday = new Date(yyyy, mm, dd);
   
     return Math.floor(((Date.parse(date) - Date.parse(isBirthday)) / 60 / 60 / 24 / 365 / 1000));
    };
    newUser.getLogin = function () {
  
      return `${(this.firstName.charAt(0) + this.lastName).toLowerCase()}`;
    };

  return newUser;
 }
 
let newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.getAge());